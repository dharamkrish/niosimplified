package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FireFoxNewPage extends BasePage{
	//*********Constructor*********
	public FireFoxNewPage (WebDriver driver) {
		super(driver);
	}

	//*********Page Variables*********
	String newFirefoxPageURL = "https://www.mozilla.org/en-US/firefox/new/";

	String OtherLanguageExpectedURL = "https://www.mozilla.org/en-US/firefox/all/";

	//*********Web Elements*********
	// WebElement searchBox = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[1]/div/div[1]/input"));
	WebDriverWait wait = new WebDriverWait(driver, 20);

	public boolean verifyIfInNewFirefoxPage (){
		driver.get(newFirefoxPageURL);
		String currUrl= driver.getCurrentUrl();
		Assert.assertEquals(currUrl, newFirefoxPageURL);
		return true;

	}
	public boolean goToAllLangPage () throws Exception{
		try {
			OtherLanguageExpectedURL = "https://www.mozilla.org/en-US/firefox/all/";
			WebElement otherLangLink = driver.findElement(By.xpath("//*[@id=\"other-platforms-languages-wrapper\"]/li[2]/a"));
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(otherLangLink)).click();
			System.out.println("clicked other language");
			Thread.sleep(9000);
			String ActualUrl = driver.getCurrentUrl();
			Assert.assertEquals(ActualUrl, OtherLanguageExpectedURL);
			System.out.println("in other language page");


		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

}
