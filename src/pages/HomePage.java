package pages;

	 
	import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import sun.rmi.runtime.Log;
	 
	public class HomePage extends BasePage {
	 
	    //*********Constructor*********
	    public HomePage (WebDriver driver) {
	        super(driver);
	    }
	 
	    //*********Page Variables*********
	    String baseURL = "http://www.google.com/";
	 
	    //*********Web Elements*********
	   // WebElement searchBox = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[1]/div/div[1]/input"));
	  
	  
	  
	   
	    // get the "Add Item" element
	    
	    
	   // WebElement searchBox = driver.findElement(By.xpath("//*[@id='q']"));
	    /*WebElement typeAhead = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[2]/div[2]/ul/li[1]/div[1]/div/span"));
	    WebElement DownloadFirefox = driver.findElement(By.xpath("//h3/a[@href='https://www.mozilla.org/en-US/firefox/new/']"));	*/	
	    String newMozillaUrl ="https://www.mozilla.org/en-US/firefox/new/";
	  
	 
	    //*********Page Methods*********
	    //Go to Homepage
	    
	    public void launchGoogle (){
	    	String expURL = "https://www.google.com/";
	        driver.get("https://www.google.com");
	      //  driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	        this.verifyUrl(expURL);	  
	        System.out.println("URL verification Done ");
	    }
	    public void verifySearchBoxInputText() {
	    	WebElement searchBox = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[1]/div/div[1]/input"));
	    	try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	if(searchBox.isEnabled()) {
	    		searchBox.sendKeys("Mozilla");
	    	}	
	    }
	    
	    public void clickTypeAheadDownloadNavigatetoNewPage() throws Exception{
	    	
	    	try {
	    //	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	    	
			Thread.sleep(4000);
	    	//*[@id="tsf"]/div[2]/div/div[2]/div[2]/ul/li[3]/div[1]/div/span/b
	    	WebElement typeAhead = driver.findElement(By.xpath( "//span[.='mozilla']")) ;
	    	System.out.println("found TypeAheand");
	    	//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
            Thread.sleep(4000);
	    	if(typeAhead.isDisplayed() && typeAhead.isEnabled()) {
	    		typeAhead.click();
	    	}
	    	Thread.sleep(4000);
	    	WebElement downloadFirefoxLink =  driver.findElement(By.xpath( "//*[@id=\'rso\']/div[1]/div/div/table/tbody/tr[1]/td[1]/div/span/h3/a")) ;
	    	System.out.println("found downloadFirefoxLink");
	    	if(downloadFirefoxLink.isDisplayed() && downloadFirefoxLink.isEnabled()) {
	    		downloadFirefoxLink.click();
	    	}
	    	/*WebElement myDynamicElement = (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(LongHornProcessOrchestrator)));*/
	    	String title = driver.getTitle();
	    	Assert.assertEquals(title, "Download Firefox — Free Web Browser — Mozilla");
	    
	     		
	    	} catch(Exception ex) {
	    		ex.printStackTrace();
	    	}
	    	
	    }
	    
	 
	    //Go to LoginPage
	    
	}
	

