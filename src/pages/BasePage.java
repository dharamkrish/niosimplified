package pages;
 
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
 
public class BasePage {
    public WebDriver driver;
    public WebDriverWait wait;
 
    //Constructor
    public BasePage (WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,15);
    }
 
    //Wait Wrapper Method
    public void waitVisibility(By elementBy) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }
 
    //Click Method
    public void click (By elementBy) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
    }
 
    //Write Text
    public void writeText (By elementBy, String text) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).sendKeys(text);
    }
 
    //Read Text
    public String readText (By elementBy) {
        waitVisibility(elementBy);
        return driver.findElement(elementBy).getText();
    }
 
    //Assert
    public void assertEquals (By elementBy, String expectedText) {
        waitVisibility(elementBy);
        Assert.assertEquals(readText(elementBy), expectedText);
 
    }
    
    //
    public void verifyUrl(String expectedURL) {
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		String currentURL = driver.getCurrentUrl();
		Assert.assertEquals(currentURL, expectedURL);
	}
	public boolean isElementVisible(By elementLocation){
		return driver.findElement(elementLocation).isDisplayed();
	}

	// is element enabled
	public boolean isElementEnabled(By elementLocation){
		return driver.findElement(elementLocation).isEnabled();
	}

	// Wait for element to be visible giving it 20 seconds, feel free to change this.
	public void waitTillVisiable(By elementLocation, String text){
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.textToBePresentInElementValue(elementLocation, text));
	}

	// Wait for element to be clickable. Giving it 5 seconds, feel free to change this
	public void waitTillClickable(By elementLocation){
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(elementLocation));
		WebElement element = driver.findElement(elementLocation);
		Assert.assertTrue(element.isEnabled());
	}
/*
 * verify if a page is loaded by checking the page for certain elements to be displayed on the page
 */
	public boolean verifyPageLoaded(String propertiesXpath, String generalTxtXpath) throws Exception{
		try{
		//System.out.println("VERIFYING IF WE ARE IN THE RIGHT PAGE");
		
		WebDriverWait wait = new WebDriverWait(driver, 9);
		WebElement a = driver.findElement(By.xpath(propertiesXpath));
		//	WebElement b = driver.findElement(By.xpath(generalTxtXpath));
		Assert.assertTrue(a.isDisplayed() /* && b.isDisplayed()*/, "elements are not visible. Loading  expected page failed");
		} catch (Exception ex){
			//ex.printStackTrace();
			return false;
		}
     return true;
	}
    
    
    
} // class ends