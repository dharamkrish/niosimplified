package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FirefoxAllEnUS extends BasePage{
	//*********Constructor*********
	public FirefoxAllEnUS (WebDriver driver) {
		super(driver);
	}
	String OtherLanguageURL = "https://www.mozilla.org/en-US/firefox/all/";
	WebDriverWait wait = new WebDriverWait(driver, 20);

	public boolean verifyIfInAllLangPage (){
		driver.get(OtherLanguageURL);
		String currUrl= driver.getCurrentUrl();
		Assert.assertEquals(currUrl, OtherLanguageURL);
		return true;  
	}

	public boolean listAllLanguages() throws Exception{
		List <WebElement> langlist = new ArrayList<WebElement>();
		int iCount = 0;
		String localLangTxt=null;
		Thread.sleep(9000);
		iCount  = driver.findElements(By.xpath("//tbody/tr")).size();  
		Assert.assertEquals(iCount, 99);
		for(int i = 1 ; i <= iCount ; i++) {
			localLangTxt = driver.findElement(By.xpath("//tbody/tr["+i+"]/th/strong")).getText();   
			System.out.println(localLangTxt);
		}
		return true;	  
	}

}
