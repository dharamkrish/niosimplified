package tests;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import pages.FireFoxNewPage;;

public class NewFirefoxPageTests extends BaseTest{
	@Test (priority = 0)
	public void testNewFirefoxPage() throws Exception{
		try{
			//*************PAGE INSTANTIATIONS*************
			FireFoxNewPage fireHome = new FireFoxNewPage(driver);  
			//*************PAGE METHODS********************
			fireHome.verifyIfInNewFirefoxPage();
			String expURL = "https://www.mozilla.org/en-US/firefox/new/";
			String ActualURL = driver.getCurrentUrl();
			Assert.assertEquals(ActualURL, expURL);
		}catch(Exception ex) {
			ex.printStackTrace();
		}

	}	
	@Test (priority = 0)
	public void testgoToAllLanguages() throws Exception{
		try {
			boolean testPass = false;
			FireFoxNewPage fireHome = new FireFoxNewPage(driver);  
			if(fireHome.verifyIfInNewFirefoxPage()) {
				testPass = fireHome.goToAllLangPage(); 
				Assert.assertTrue(testPass);
			}


		}catch (Exception ex) {
			ex.printStackTrace();

		}
	}

}