package tests;
import org.testng.Assert;
import org.testng.annotations.Test;

import pages.HomePage;

public class GoogleHomeTest extends BaseTest{
	 @Test (priority = 0)
	    public void testGoogleHomepage() throws Exception{
	       try{
	        //*************PAGE INSTANTIATIONS*************
	        HomePage homePage = new HomePage(driver);
	 
	        //*************PAGE METHODS********************
	        homePage.launchGoogle();
	        homePage.verifySearchBoxInputText();
	        homePage.clickTypeAheadDownloadNavigatetoNewPage();
	        String expURL = "https://www.mozilla.org/en-US/firefox/new/";
	        String ActualURL = driver.getCurrentUrl();
	        Assert.assertEquals(ActualURL, expURL);
	 }catch(Exception ex) {
		 ex.printStackTrace();
	    }

	 }	 
	    
}
