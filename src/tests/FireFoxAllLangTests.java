package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import pages.FireFoxNewPage;
import pages.FirefoxAllEnUS;

public class FireFoxAllLangTests extends BaseTest{
	@Test (priority = 0)
	public void verifyIFInAllFirefoxPage() throws Exception{
		try{
			//*************PAGE INSTANTIATIONS*************
			FirefoxAllEnUS fireAllLang = new FirefoxAllEnUS(driver);  
			//*************PAGE METHODS********************
			fireAllLang.verifyIfInAllLangPage();
			String OtherLanguageURL = "https://www.mozilla.org/en-US/firefox/all/";
			String ActualURL = driver.getCurrentUrl();
			Assert.assertEquals(ActualURL, OtherLanguageURL);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}	
	@Test
	public void testList() throws Exception{
		try {
			boolean isPass=false;
			FirefoxAllEnUS fireAllLang = new FirefoxAllEnUS(driver);
			if(fireAllLang.verifyIfInAllLangPage()) {
				isPass=fireAllLang.listAllLanguages();
			}
			Assert.assertTrue(isPass);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
